<?php

	require "config.php";
	$link = mysql_connect($config['host'],$config['user'],$config['pass']) or die(mysql_error());

	mysql_query("DROP DATABASE IF EXISTS {$config['database']}") or die(mysql_error());

	mysql_query("CREATE DATABASE ".$config['database']) or die(mysql_error());
	mysql_select_db($config['database'],$link) or die(mysql_error());

	echo "database {$config['database']} was created <br>";

	// Создаем таблицу постов в ней
	mysql_query("CREATE TABLE {$config['postsTable']} (
			  ID int,
		      THREAD int,
		      OWNER varchar(25),
		      TITLE varchar(255),
		      TEXT varchar(8196),
		      EMAIL varchar(255),
		      TIME varchar(10),
		      DATA varchar(25),
		      IMG varchar(50)
	)") or die(mysql_error());
	echo "table {$config['postsTable']} was created <br>";

	// Создаем таблицу тредов в ней
	mysql_query("CREATE TABLE {$config['threadsTable']} (
		      ID int,
		      TIME int,
		      HASH varchar(255)
	)") or die(mysql_error());
	echo "table {$config['threadsTable']} was created <br>";

	// Создаем таблицу логов в ней
	mysql_query("CREATE TABLE {$config['logTable']} (
		        IP varchar(255),
		        USERAGENT varchar(255),
		        SESSION varchar(255),
		        TIME varchar(255),
		        ACTION varchar(255)
	)") or die(mysql_error());
	echo "table {$config['logTable']} was created <br>";

?>